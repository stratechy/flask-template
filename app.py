import os
from project import create_app
from config import Config

basedir = os.path.abspath(os.path.dirname(__file__))
config = Config(basedir)
app = create_app(config)
