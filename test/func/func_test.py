from project.exampleblueprint import ExampleTable


def test_view(client):
    '''
    GIVEN the test app
    WHEN the '/' page is requested
    THEN check that the response is valid
    '''
    response = client.get('/')
    assert response.status_code == 200
    assert b"Example Page Content Block" in response.data


def test_post(client, valid_data):
    '''
    GIVEN the test app & valid data
    WHEN the data is posted
    THEN check that the data is added to the db
    '''
    for entry in valid_data:
        data = dict(field=entry.thing, follow_redirects=True)
        client.post('/', data=data)
    query = ExampleTable.query.all()

    assert len(query) == len(valid_data)

    for i in range(len(query)):
        assert query[i].thing == valid_data[i].thing
