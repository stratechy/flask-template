from project.exampleblueprint import ExampleTable
from project import db


def db_add(data):
    # Helper function to add data to db
    for entry in data:
        db.session.add(entry)
    db.session.commit()


def test_entry(valid_data):
    '''
    GIVEN the ExampleTable model
    WHEN a new entry is created
    THEN check that the model holds the correct data
    '''
    assert valid_data[0].thing == 'asdf'
    assert valid_data[1].thing == 'qwer'


def test_db(valid_data, app_setup):
    '''
    GIVEN a number of SleepEntry records
    WHEN the db is initialized and records are added
    THEN check records are added successfully
    '''
    db_add(valid_data)
    query = ExampleTable.query.all()
    assert len(query) == 2
    assert query[0].thing == 'asdf'
    assert query[1].thing == 'qwer'
