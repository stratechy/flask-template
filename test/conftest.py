import pytest
from app import basedir
from project import create_app, db
from config import TestConfig
from project.exampleblueprint import ExampleTable


@pytest.fixture(scope='module')
def valid_data():
    valid_data = []
    valid_data.append(ExampleTable(thing='asdf'))
    valid_data.append(ExampleTable(thing='qwer'))

    return valid_data


@pytest.fixture(scope='module')
def invalid_data():
    invalid_data = []
    invalid_data.append(ExampleTable(thing=1))

    return invalid_data


@pytest.fixture(scope='module')
def app_setup():
    # Apply test configuration options
    config = TestConfig(basedir=basedir)
    app = create_app(config)

    # Create application context for Flask-SQAlchemy
    with app.app_context():
        db.create_all()

        yield app

        db.drop_all()


@pytest.fixture(scope='module')
def client(app_setup):
    # Create test client for accessing views
    with app_setup.test_client() as client:
        yield client
