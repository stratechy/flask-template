import os


# Default values that should not be used in production
development_options = {
    'DEBUG': True,
    'SECRET_KEY': 'terrible_secret_key'
}


class Config(object):

    def __init__(self, basedir):
        # Set default values that are suitable for production
        db_path = 'sqlite:///' + os.path.join(basedir, 'instance/app.db')
        self.SQLALCHEMY_DATABASE_URI = db_path
        self.SQLALCHEMY_TRACK_MODIFICATIONS = False
        self.WTF_CSRF_ENABLED = True

        '''  Iterates through config options, and replaces default
        values with environment variables where possible '''
        unset_options = []
        for option in development_options:
            # Use environment variable
            if os.environ.get(option) is not None:
                setattr(self, option, os.environ.get(option))
            # Otherwise, revert to default value
            else:
                setattr(self, option, development_options[option])
                unset_options.append(option)
        # Warn if using values inappropriate for production
        if len(unset_options) > 0:
            print('Config Warning: Missing environment variables.'
                  ' Not suitable for production use')


class TestConfig(Config):
    def __init__(self, basedir, *args, **kwargs):
        ''' Loads standard config, then overrides testing specific values '''
        super().__init__(basedir)
        # Disable error catching during request handling
        self.TESTING = True
        # Disable CSRF
        self.WTF_CSRF_ENABLED = False
        # Use db specific to testing
        db_path = 'sqlite:///' + os.path.join(basedir, 'instance/testing.db')
        self.SQLALCHEMY_DATABASE_URI = db_path
