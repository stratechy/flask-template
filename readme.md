# Webapp Template

A starting point for a flask webapp.  I prefer a structured workflow, hence the baseline example and writeup.

## Usage

This file structure is intended to be conducive to modularity, blueprint usage and test driven development.  Mostly original work, but many elements inspired by a couple of examples given below.


## Setup
To install production packages:

```
pipenv install
```

Or to install dev packages:

```
pipenv install --dev
```

### Database

```
flask db init
flask db migrate -m "Comment here"
flask db upgrade
```

### Package App as an Editable Package

```
pipenv install -e .
```

## Testing

### Pytest

```
python -m pytest
```

## License

Code is provided under the MIT License.  See [LICENSE](/LICENSE.txt) for details.

Inspiration from [microblog](https://github.com/miguelgrinberg/microblog), [flaskr-tdd](https://github.com/mjhea0/flaskr-tdd), plus the official docs for [flask](https://flask.palletsprojects.com/), [flask-wtf](https://flask-wtf.readthedocs.io/en/stable/) / [wtforms](https://wtforms.readthedocs.io/), [flask-sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/) / [sqlalchemy](docs.sqlalchemy.org/en/latest/), [flask-migrate](https://flask-migrate.readthedocs.io/en/latest/) / [alembic](https://alembic.sqlalchemy.org/en/latest/).
