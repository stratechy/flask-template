from flask import Blueprint                                  # Blueprint Import
from flask import render_template, redirect, url_for         # Route Import
from wtforms import SubmitField, StringField                 # Form Import
from wtforms.validators import DataRequired                  # Form Import
from flask_wtf import FlaskForm                              # Form Import
from project import db                                       # Database Import


bp = Blueprint('bpname', __name__, template_folder='templates')


class ExampleForm(FlaskForm):
    field = StringField('FieldName', validators=[DataRequired()])
    submit = SubmitField('Submit')

    def newentry(self):
        ''' Add form values to database '''
        data = ExampleTable(thing=self.field.data)
        db.session.add(data)
        db.session.commit()


class ExampleTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    thing = db.Column(db.String(256))


@bp.route('/', methods=['GET', 'POST'])
def example():
    form = ExampleForm()
    if form.validate_on_submit():
        form.newentry()
        return redirect(url_for('bpname.example'))
    return render_template('example.html', form=form)
