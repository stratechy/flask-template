from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


# CONFIGURATION #

''' Create Flask extension instances so they can be
bound to the Flask application instance later '''


db = SQLAlchemy()
migrate = Migrate()


def create_app(config):
    ''' Application factory '''
    app = Flask(__name__)
    app.config.from_object(config)
    initialize_extensions(app)
    register_blueprints(app)
    return app


def initialize_extensions(app):
    ''' Bind each extension to the Flask application instance '''
    db.init_app(app)
    migrate.init_app(app, db)


def register_blueprints(app):
    ''' Register each Blueprint with the Flask application instance '''
    from project.exampleblueprint import bp
    app.register_blueprint(bp)
